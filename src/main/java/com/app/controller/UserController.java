package com.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.User;
import com.app.exception.UserNotFountException;
import com.app.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService service;
	
	
	
	@PostMapping("/save")
	public ResponseEntity<User> saveUser(@RequestBody @Valid User user)
	{
		logger.info("user '"+user.getUserId()+"' created ");
		return new ResponseEntity<User>(service.saveUser(user),HttpStatus.CREATED);
	}
	
	
	@GetMapping("/getAll")
	public ResponseEntity<List<User>> getAllUsers()
	{
		return ResponseEntity.ok(service.getAllUsers());
	}
	
	
	@GetMapping("/{userId}")
	public ResponseEntity<User> getUser(@PathVariable int userId) throws UserNotFountException
	{
		return ResponseEntity.ok(service.getUser(userId));
	}
}
