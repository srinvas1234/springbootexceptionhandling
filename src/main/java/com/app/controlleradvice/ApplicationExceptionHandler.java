package com.app.controlleradvice;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.app.exception.UserNotFountException;

@RestControllerAdvice
public class ApplicationExceptionHandler {
	
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)

	@ExceptionHandler(MethodArgumentNotValidException.class)

	public Map<String,String> handleInvalidArgument(MethodArgumentNotValidException ex)
	{
		Map<String,String> errorMap=new HashMap<String, String>();
		ex.getBindingResult().getFieldErrors().forEach(error -> {
			errorMap.put(error.getField(), error.getDefaultMessage());
		});
		return errorMap;
	}
	
	
	
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserNotFountException.class)
	public Map<String,String> handleBusinessException(UserNotFountException exception)
	{
		Map<String,String> errorMap=new LinkedHashMap<String, String>();
		
		errorMap.put("errorMessage", exception.getMessage());
		return errorMap;
	}
	
	
	
	
}
