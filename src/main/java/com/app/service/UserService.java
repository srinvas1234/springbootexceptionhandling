package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.entity.User;
import com.app.exception.UserNotFountException;
import com.app.repository.UserRespository;

@Service
public class UserService {
	
	@Autowired
	private UserRespository respository;
	
	public User saveUser(User user)
	{
	return respository.save(user);	
	}
	
	public List<User> getAllUsers()
	{
		return respository.findAll();
	}
	public User getUser(int userId) throws UserNotFountException {
		
		User user=respository.findByUserId(userId);
		if(user!=null)
		{
			return user;
		}
		throw new UserNotFountException(String.format("User %d Not Found", userId));
	}
	
	
}
