package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entity.User;

public interface UserRespository extends JpaRepository<User, Integer>{

	
	User findByUserId(int userId);
}
