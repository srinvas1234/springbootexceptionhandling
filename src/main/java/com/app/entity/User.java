package com.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@Entity
@Table(name = "userTab505")
public class User {
	@Id
	@GeneratedValue
	private int userId;
	
	@NotBlank(message = "username shouldn't be null")
	private String name;
	
	@Email(message = "invalid email address")
	private String email;
	
	@Pattern(regexp = "^\\d{10}$",message = "Invalid mobile Number entered")
	private String mobileNum;
	
	private String gender;
	
	@Min(18)
	@Max(60)
	private int age;
	
	@NotBlank
	private String nationality;
	
	
}
