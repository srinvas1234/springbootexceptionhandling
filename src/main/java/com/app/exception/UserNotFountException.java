package com.app.exception;


public class UserNotFountException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFountException(String message) {
		super(message);
	}

	
	
}
